const sr = Scrollreveal();

ScrollReveal.reveal('image-group-projects', {
    origin: 'right',
    distance: '50px',
    duration: 2000
});