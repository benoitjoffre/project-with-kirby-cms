<?php snippet('header') ?>
    <ul class="projects">
        <?php foreach ($page->children()->listed() as $project): ?>
        <li>
            <a href="<?= $project->url() ?>">
                <div class="image-group-projects">
                    <?= $project->image()->crop(500) ?>
                </div>
                
                <?= $project->title() ?>
            </a>
        </li>
        <?php endforeach ?>
    </ul>
   
<?php snippet('footer') ?>