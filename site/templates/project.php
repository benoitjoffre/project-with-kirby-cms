<?php snippet('header') ?>
    <ul class="gallery">
    <p class="paragraph"><?= $page->text() ?></p>
        <?php foreach ($page->images() as $image): ?>
        
        <li>
            <a href="<?= $image->url() ?>">
            <div class="image-project">
                <?= $image->resize(1200)?>
                
            </div>
            </a>
        </li>
       
        <?php endforeach ?>
    </ul>

<?php snippet('footer') ?>