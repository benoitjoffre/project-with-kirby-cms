<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $site->title() ?></title>

    <?= css('index.css') ?>

</head>
<body>
    <header>
        <a class="logo" href="<?= $site->url() ?>"><?= $site->title() ?></a>

        <nav class="menu">
            <?php foreach ($site->children()->listed() as $subpage): ?>
                <a href="<?= $subpage->url() ?>"><?= $subpage->title() ?></a>
            <?php endforeach ?>
        </nav>
    </header>
    <h1 class="title-page"><?= $page->title() ?></h1>
    
    
